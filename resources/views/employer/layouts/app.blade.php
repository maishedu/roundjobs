<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@yield('title') | {{config('app.name')}} </title>
    <link rel="shortcut icon" href="{{ getSettingValue('favicon') }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-197548708-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-197548708-1');
    </script>
    <meta name="google-site-verification" content="vK6JSEGNOWLgtuhoMVBzymAndkJyJ6Yt5LewWO56FsU" />
    <meta property="og:title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk">
    <meta property="og:site_name" content="hospitalityhire">
    <meta property="og:url" content="https://hospitalityhire.co.uk/">
    <meta property="og:description" content="Hospitalityhire. Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK. Chef, Kitchen staff, Take away, Desert Parlor & Coffee Shop Jobs in UK. Student Internship & Placement and part time jobs">
    <meta property="og:type" content="article">
    <meta property="og:image" content="https://hospitalityhire.co.uk/uploads/settings/155/Only-Hospitality-Jobs-%283%29.png">
    <link rel="canonical" href="https://hospitalityhire.co.uk/" />
    <meta name="keywords" content=" hospitality jobs, hospitality and catering, full time and part time jobs, internships, Placement’s assistance, full time jobs, jobs near me full time, working part time, pubs and bars, hospitality career, job vacancy for graduates, restaurants jobs near me, hospitality jobs near me, coffee shop jobs, hospitality recruitment, restaurant openings near me">
    <meta name="twitter:title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk " />
    <meta name="twitter:description" content="HospitalityHire.co.uk. Start First Career Break. Part time & Full Time Hospitality Hiring. Hotel, Pub, Bar & Coffee Shop Jobs. Internship & Placement Assistance " />
    <meta itemprop="title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk" />
    <meta itemprop="description" content="HospitalityHire.co.uk. Start First Career Break. Part time & Full Time Hospitality Hiring. Hotel, Pub, Bar & Coffee Shop Jobs. Internship & Placement Assistance "/>
    <meta property="og:title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk">
    <meta property="og:site_name" content="hospitalityhire">
    <meta property="og:url" content="https://hospitalityhire.co.uk/">
    <meta property="og:description" content=" ">
    <meta property="og:type" content="website">
    <meta property="og:image" content="">

    <!-- General CSS Files -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link href="{{ asset('assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css"/>

@stack('css')

<!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/web/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/web/css/components.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link href="{{ mix('assets/css/infy-loader.css') }}" rel="stylesheet" type="text/css"/>

</head>
<body class="layout-3">
<div id="app">
    <div class="infy-loader" id="overlay-screen-lock">
        @include('loader')
    </div>
    <div class="main-wrapper container">
    @include('employer.layouts.header')
    @include('employer_profile.edit_profile_modal')
    @include('employer_profile.change_password_modal')

    <!-- Main Content -->
        <div class="main-content">
            @yield('content')
        </div>
        <footer class="main-footer">
            @include('layouts.footer')
        </footer>
    </div>
</div>
<script src="{{ asset('messages.js') }}"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('assets/web/js/stisla.js') }}"></script>
<script src="{{ asset('assets/web/js/scripts.js') }}"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script>
    (function ($) {
        $.fn.button = function (action) {
            let currentLocale = "{{ Config::get('app.locale') }}";
            Lang.setLocale(currentLocale);
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };
    }(jQuery));
    $(document).ready(function () {
        $('.alert').delay(5000).slideUp(300);
    });
    $('[data-dismiss=modal]').on('click', function (e) {
        var $t = $(this),
            target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

        $(target).modal("hide");
    });
</script>
@stack('scripts')
<script>
    let profileUrl = "{{ url('employer/employer-profile') }}";
    let profileUpdateUrl = "{{ url('employer/employer-profile-update') }}";
    let updateLanguageURL = "{{ url('update-language')}}";
    let changePasswordUrl = "{{ url('employer/employer-change-password') }}";
    let loggedInUserId = "{{ getLoggedInUserId() }}";
    let defaultImageUrl = "{{ asset('assets/img/infyom-logo.png') }}";
    let readAllNotifications = "{{ url('employer/read-all-notification') }}";
    let readNotification = "{{ url('employer/notification') }}";
</script>
<script src="{{ mix('assets/js/employer_profile/employer_profile.js') }}"></script>
<script src="{{ asset('js/currency.js') }}"></script>
</body>
</html>
