@extends('web.layouts.app')
@section('title')
    {{ __('Advertise') }}
@endsection
@section('content')
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ __('Advertise with us') }}</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="about-us ptb80">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h3 class="text-purple">{{ __('Advertise with us') }}</h3>
                    <p class="pt30" style="font-size: 20px !important;"> We are supporting Local businesses and help job seeker to find a job. Our welcome pack comes with free registration and 10 job posting. Register Today and start advertising in 5 Minutes.</p><br>
                    <h3>Advertise with us for free</h3>
                </div>
                <div >

                    <a href="{{ route('employer.register') }}"  class="btn btn-orange btn-xl active" role="button" aria-pressed="true">Register Today</a>

                    <a href="{{ route('login') }}"  class="btn btn-orange btn-xl active" role="button" aria-pressed="true">Login</a>
                </div>
            </div>
        </div>
    </section>
    <section class="about-process ptb80">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <div class="section-title">
                        <h2>{{ __('Simple and Effective Recruitment Solutions for Everyone') }}</h2>
                    </div>
                </div>

                <!-- Start of First Column -->
                <div class="col-md-3 col-xs-12 text-center">
                    <div class="process-icon">
                        <img src="{{ asset('uploads/icons/recruitment.svg') }}" width="100" height="100" class="img-fluid" alt="Responsive image">
                    </div>
                    <h3 class="pb20">Recruitment Agencies</h3>

                </div>
                <!-- End of First Column -->


                <!-- Start of Second Column -->
                <div class="col-md-3 col-xs-12 text-center">
                    <div class="process-icon">
                        <img src="{{ asset('uploads/icons/direction.svg') }}" width="100" height="100"class="img-fluid" alt="Responsive image">
                    </div>

                    <h3 class="pb20">Direct Employers</h3>
                </div>
                <!-- End of Second Column -->


                <!-- Start of Third Column -->
                <div class="col-md-3 col-xs-12 text-center">
                    <div class="process-icon">
                        <img src="{{ asset('uploads/icons/marketing.svg') }}" width="100" height="100" class="img-fluid" alt="Responsive image">
                    </div>
                    <h3 class="pb20">Advertising Agencies</h3>
                </div>
                <!-- End of Third Column -->
                <!-- Start of Fourth Column -->
                <div class="col-md-3 col-xs-12 text-center">
                    <div class="process-icon">
                        <img src="{{ asset('uploads/icons/corporation.svg') }}" width="100" height="100" class="img-fluid" alt="Responsive image">
                    </div>

                    <h3 class="pb20">Corporates</h3>
                </div>


            </div>
        </div>
    </section>
@endsection
