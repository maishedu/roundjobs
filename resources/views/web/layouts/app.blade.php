@php
    $settings  = settings();
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Mobile viewport optimized -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">

    <!-- Meta Tags - Description for Search Engine purposes -->
    <meta name="description" content="{{config('app.name')}}">
    <meta name="keywords"
          content="{{config('app.name')}}">
    <link rel="shortcut icon" href="{{ asset($settings['favicon'])}}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-197548708-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-197548708-1');
    </script>
    <meta name="google-site-verification" content="vK6JSEGNOWLgtuhoMVBzymAndkJyJ6Yt5LewWO56FsU" />
    <meta property="og:title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk">
    <meta property="og:site_name" content="hospitalityhire">
    <meta property="og:url" content="https://hospitalityhire.co.uk/">
    <meta property="og:description" content="Hospitalityhire. Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK. Chef, Kitchen staff, Take away, Desert Parlor & Coffee Shop Jobs in UK. Student Internship & Placement and part time jobs">
    <meta property="og:type" content="article">
    <meta property="og:image" content="https://hospitalityhire.co.uk/uploads/settings/155/Only-Hospitality-Jobs-%283%29.png">
    <link rel="canonical" href="https://hospitalityhire.co.uk/" />
    <meta name="keywords" content=" hospitality jobs, hospitality and catering, full time and part time jobs, internships, Placement’s assistance, full time jobs, jobs near me full time, working part time, pubs and bars, hospitality career, job vacancy for graduates, restaurants jobs near me, hospitality jobs near me, coffee shop jobs, hospitality recruitment, restaurant openings near me">
    <meta name="twitter:title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk " />
    <meta name="twitter:description" content="HospitalityHire.co.uk. Start First Career Break. Part time & Full Time Hospitality Hiring. Hotel, Pub, Bar & Coffee Shop Jobs. Internship & Placement Assistance " />
    <meta itemprop="title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk" />
    <meta itemprop="description" content="HospitalityHire.co.uk. Start First Career Break. Part time & Full Time Hospitality Hiring. Hotel, Pub, Bar & Coffee Shop Jobs. Internship & Placement Assistance "/>
    <meta property="og:title" content="Best Hospitality, Restaurant, Latest Hotel, Bar & Pub Jobs in UK- Hospitalityhire.co.uk">
    <meta property="og:site_name" content="hospitalityhire">
    <meta property="og:url" content="https://hospitalityhire.co.uk/">
    <meta property="og:description" content=" ">
    <meta property="og:type" content="website">
    <meta property="og:image" content="">

    <!-- Website Title -->
    <title>@yield('title') | {{config('app.name')}} </title>

    <!-- Google Fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:300,400,400i,700,800|Varela+Round" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">

    <!-- CSS links -->
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/custom.css') }}">
    <link rel="stylesheet" href="{{ mix('assets/css/flex.css') }}">
    <link rel="stylesheet" href="{{ mix('assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ mix('assets/css/custom-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">
    @livewireStyles

@yield('page_css')
@yield('css')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('web/js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<!-- Header Start -->
@include('web.layouts.header')
<!-- Header End -->

<!-- Main Content Start -->
@yield('content')
<!-- Main Content End -->

<!-- Footer Start -->
@include('web.layouts.footer')
<!-- Footer End -->

<!-- ===== All Javascript at the bottom of the page for faster page loading ===== -->
<script src="{{ asset('web/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('web/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('web/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('web/js/swiper.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.countTo.js') }}"></script>
<script src="{{ asset('web/js/jquery.inview.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('web/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('web/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('web/js/countdown.js') }}"></script>
<script src="{{ asset('web/js/isotope.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ asset('web/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script>
    (function ($) {
        $.fn.button = function (action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };
    }(jQuery));
    $(document).ready(function () {
        $('.alert').delay(5000).slideUp(300);
    });
    $('[data-dismiss=modal]').on('click', function (e) {
        var $t = $(this),
            target = $t[0].href || $t.data('target') || $t.parents('.modal') || [];

        $(target).modal('hide');
    });
    let createNewLetterUrl = "{{ route('news-letter.create') }}";
</script>
<script src="{{ mix('assets/js/web/js/news_letter/news_letter.js') }}"></script>
@livewireScripts


@yield('page_scripts')
@yield('scripts')

</body>
</html>
